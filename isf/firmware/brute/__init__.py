from requests.auth import HTTPDigestAuth
from pkgutil import get_data
from tqdm import tqdm
import telnetlib
import requests
import ftplib

class Brute:

    ip = '127.0.0.1'
    port = 1488
    logins = []
    passwords = []


    def __init__(self, login_string, logins_file, password_string, passwords_file, ip):

        self.ip = ip
        self.logins_file = logins_file
        self.passwords_file = passwords_file
        if login_string != 'NOGODPLEASENO':
            self.logins = [login_string]
        else:
            logins_file = get_data('isf.firmware.brute.resources', self.logins_file).decode()
            self.logins = logins_file.split('\n')
        if password_string != 'NOGODPLEASENO':
            self.passwords = [password_string]
        else:
            passwords_file = get_data('isf.firmware.brute.resources', self.passwords_file).decode()
            self.passwords = passwords_file.split('\n')

        return

    def __del__(self):
        pass


    def http(self, port, uri='/'):
        url = 'http://{}:{}{}'.format(self.ip,self.port,uri)
        for login in self.logins:
            for password in self.passwords:
                for m in tqdm(range(len(self.logins) * len(self.passwords))):
                    requests.get(url, auth=HTTPDigestAuth(login, password))

    def ftp(self):
        try:
            for login in self.logins:
                for password in self.passwords:
                    for m in tqdm(range(len(self.logins)*len(self.passwords))):
                        #print(len(self.passwords))
                        #print('Try login:{}, pass:{}'.format(login, password))
                        try:
                            ftp = ftplib.FTP(self.ip, login, password)
                            print('Success. Login:{}, pass:{}'.format(login, password))
                        except ftplib.error_perm as err:
                            a = 0
        except ConnectionRefusedError:
            print("Connection refused")
        print("FTP brute is done")


    def telnet(self):
        console = b'>'
        for login in self.logins:
            for password in self.passwords:
                for m in tqdm(range(len(self.logins)*len(self.passwords))):
                    try:
                        print("pop")
                        tn = telnetlib.Telnet(self.ip,timeout=1)
                        output = tn.read_until(b'Username', timeout=0.1)
                        print("Try login:{}, pass:{}".format(login,password))
                        if output.find(console) == -1:
                            tn.write(login.encode('ascii') + b'\n')
                            tn.read_until(b'Password:', timeout=0.1)
                            tn.write(password.encode('ascii') + b'\n')
                            sec_output = tn.read_until(b'>', timeout=0.1)
                            if sec_output.find(console) != -1:
                                print("Success. Login:{}, Pass:{}".format(login, password))
                        else:
                            print("No login/pass")
                    except EOFError:
                        print("Connection refused")
                    except:
                        print("Telnet on the server is disabled")
                        return 0

        print("Telnet brute is done")